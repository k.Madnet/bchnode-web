---
layout: layout.html
---

<% set('title', 'Announcing Bitcoin Cash Node v26.0.0') %>
<% set('date', '08 January 2023') %>
<% set('author', 'Bitcoin Cash Node Team') %>

### Release announcement: Bitcoin Cash Node v26.0.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its major release version 26.0.0.

This release of Bitcoin Cash Node (BCHN) is marked as a major release, not due to consensus changes, but because it changes an interface (the `rpcbind` / `rpcallowip` options) for security reasons in a way that is not completely backward compatible, and therefore needs to be tagged as a major version in accordance with Semantic Versioning.

However, for other practical purposes, this version is more like a minor release, containing corrections and improvements.

If you use the `rpcbind` / `rpcallowip` configuration options, you should read the associated Release Notes to understand that change, in case you need to adapt your configuration.

BCHN users should consider an update to v25 or later prior to May 15, 2023 as mandatory. Users running v25 are not required to upgrade to v26.0.0, although we do recommend it.

The v24.x.0 software will expire on May 15, 2023, and will start to warn of the need to update ahead of time, from April 15, 2023 onward.

For the full release notes, please visit:

[https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.0.0](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.0.0)

Executables and source code for supported platforms are available at the above link, or via the download page on our project website at

[https://bitcoincashnode.org](https://bitcoincashnode.org)

For more information about the May 15, 2023 network upgrade, visit

[https://upgradespecs.bitcoincashnode.org/2023-05-15-upgrade/](https://upgradespecs.bitcoincashnode.org/2023-05-15-upgrade/)

We hope you enjoy our latest release and invite you to join us to improve Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
